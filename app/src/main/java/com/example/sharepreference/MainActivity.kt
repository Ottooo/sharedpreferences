package com.example.sharepreference

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreference:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        readInfo()
    }

    private fun init(){
        sharedPreference = getSharedPreferences("INFO" , Context.MODE_PRIVATE)


    }
    fun saveButton(view: View) {
        val email = EmailET.text.toString()
        val firstName = FirstNameET.text.toString()
        val lastName = LastNameET.text.toString()
        val age = AgeET.text.toString()
        val address = AddressET.text.toString()
        val editor = sharedPreference.edit()

        if(email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty()
            && age.isNotEmpty() && address.isNotEmpty()) {
            editor.putString("email", email)
            editor.putString("firstName", firstName)
            editor.putString("lastName ", lastName)
            editor.putString("age", age)
            editor.putString("address", address)
            editor.apply()
            Toast.makeText(this, "Your information is saved", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, "Some area is empty", Toast.LENGTH_SHORT).show()
        }
    }

    fun readInfo(){
        val email = sharedPreference.getString("email" ,"")
        val firstName  = sharedPreference.getString("firstName", "")
        val lastName = sharedPreference.getString("lastName", "")
        val age = sharedPreference.getString("age", "")
        val address = sharedPreference.getString("address", "")
        EmailET.setText(email)
        FirstNameET.setText(firstName)
        LastNameET.setText(lastName)
        AgeET.setText(age)
        AddressET.setText(address)

    }


}